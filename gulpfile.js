// Include Gulp
var gulp = require('gulp');

// Include required Gulp packages
var less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer')
    ;

// CSS task
gulp.task('css', function () {
    return gulp.src('less/base.less')
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(gulp.dest('./css'))
});

// Global build task
gulp.task('build', gulp.series(
    'css'
));

// Watch task
gulp.task('watch', function(){
    gulp.watch('less/**/*.{less,css}', gulp.series(
        'css'
    ));
});
